#!/bin/bash

# Installeer de basis pakketten
sudo pacman --noconfirm -S xorg ttf-dejavu ttf-liberation noto-fonts ttf-ubuntu-font-family ttf-font-awesome libreoffice-fresh-nl digikam simple-scan firefox-i18n-nl polkit-gnome gimp xed i3-gaps i3lock dmenu terminator rofi gmrun numlockx conky nitrogen picom nautilus clementine vlc pactl playerctl

# Installeer yay
git clone https://aur.archlinux.org/yay
sudo chown alain:alain yay/
cd yay
makepkg -si
cd

# Installeer de AUR paketten
yay -S ttf-ms-fonts google-chrome brlaser brscan4 sane pamac-tray


