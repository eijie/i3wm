# i3 config file (v4)
# Please see https://i3wm.org/docs/userguide.html for a complete reference!

#
# Alain J. Baudrez
#

#####################################################################################################################
#####################################################################################################################
#################                          Start of all the settings                                #################
#####################################################################################################################
#####################################################################################################################


# KEY DEFINITIONS TO REMEMBER

# $Mod = WINDOWS key or Super key or Mod4
# Mod1 = ALT key
# Control = CTRL key
# Shift = SHIFT key
# Escape = ESCAPE key
# Return = ENTER or RETURN key
# KP_Enter = Keypad Enter
# Pause = PAUSE key
# Print = PRINT key
# Tab = TAB key


#####################################################################################################################
#################                          Define the $mod variable/key                             #################
#####################################################################################################################

# Key to rule them all : Super(Windows) or Alt key?

# Mod4 = Windows or Super key on keyboard
# Mod1 = Alt key on keyboard

#Set Alt key
#set $mod Mod1

#set Super key
set $mod Mod4

#####################################################################################################################
#################                          Define the movements keys - variables                    #################
#####################################################################################################################


#This is setup for qwerty
set $up l
set $down k
set $left j
set $right semicolon

#This is setup for azerty
#set $up l
#set $down k
#set $left j
#set $right m


#####################################################################################################################
#################                                  Menu's                                           #################
#####################################################################################################################

# start dmenu
bindsym $mod+shift+d exec --no-startup-id dmenu_run -i -nb '#191919' -nf '#fea63c' -sb '#fea63c' -sf '#191919' -fn 'NotoMonoRegular:bold:pixelsize=14'

# gmrun
bindsym mod1+F2 exec --no-startup-id gmrun

# start rofi full
bindsym $mod+F11 exec --no-startup-id rofi -show run -fullscreen -font "Noto Sans 13"

# start rofi small
bindsym $mod+F12 exec --no-startup-id rofi -show run -font "Noto Sans 13"

bindsym $mod+Menu exec rofi -modi drun -show drun -display-drun "Apps : " -line-padding 4 \
                -columns 2 -padding 50 -hide-scrollbar \
                -show-icons -drun-icon-theme "Arc-X-D" -font "Noto Sans Regular 11"
# exit-menu
bindsym $mod+BackSpace exec ~/.config/i3/scripts/shutdown_menu -p rofi -c

#####################################################################################################################
#################                          reload changed configuration                             #################
#####################################################################################################################


# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart

# reload the configuration file
bindsym $mod+Shift+c reload

#####################################################################################################################
#################                          Stopping an application                                  #################
#####################################################################################################################

# kill focused window
bindsym $mod+Shift+q kill
bindsym $mod+q kill

#####################################################################################################################
#################                          Define the movements keys - variables                    #################
#####################################################################################################################


#This is setup for qwerty
set $up l
set $down k
set $left j
set $right semicolon

#This is setup for azerty
#set $up l
#set $down k
#set $left j
#set $right m

#####################################################################################################################
#################                          Moving around in i3                                      #################
#####################################################################################################################


# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus
bindsym $mod+$left focus left
bindsym $mod+$down focus down
bindsym $mod+$up focus up
bindsym $mod+$right focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+$left move left
bindsym $mod+Shift+$down move down
bindsym $mod+Shift+$up move up
bindsym $mod+Shift+$right move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

#####################################################################################################################
#################                          moving around workspaces                                 #################
#####################################################################################################################


# next/previous workspace

bindsym Mod1+Tab workspace next
bindsym Mod1+Shift+Tab workspace prev
bindsym $mod+Tab workspace back_and_forth

#navigate workspaces next / previous
bindsym Mod1+Ctrl+Right workspace next
bindsym Mod1+Ctrl+Left workspace prev

# switch to workspace with urgent window automatically
for_window [urgent=latest] focus

#####################################################################################################################
#################                          Tiling parameters                                        #################
#####################################################################################################################

# orientation for new workspaces
default_orientation horizontal

# split in horizontal orientation
bindsym $mod+h split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
# Super + F in arcolinux is execute thunar
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
# qwerty/azerty issue for letter z
bindsym $mod+s layout stacking
bindsym $mod+z layout tabbed
bindsym $mod+e layout toggle split


# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

#####################################################################################################################
#################                          resize                                                   #################
#####################################################################################################################

# Resizing by 1
bindsym $mod+Mod1+Ctrl+Right resize shrink width 1 px or 1 ppt
bindsym $mod+Mod1+Ctrl+Up resize grow height 1 px or 1 ppt
bindsym $mod+Mod1+Ctrl+Down resize shrink height 1 px or 1 ppt
bindsym $mod+Mod1+Ctrl+Left resize grow width 1 px or 1 ppt


####################################################################################################################
#old resize keybindings
# resize window (you can also use the mouse for that)

#bindsym $mod+r mode "resize"

#mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
 #      bindsym $left       resize shrink width 10 px or 10 ppt
 #      bindsym $down       resize grow height 10 px or 10 ppt
 #      bindsym $up         resize shrink height 10 px or 10 ppt
 #      bindsym $right      resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
 #      bindsym Left        resize shrink width 10 px or 10 ppt
 #      bindsym Down        resize grow height 10 px or 10 ppt
 #      bindsym Up          resize shrink height 10 px or 10 ppt
 #      bindsym Right       resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape
 #      bindsym Return mode "default"
 #      bindsym Escape mode "default"
#}

#####################################################################################################################
#################                          choose the font                                          #################
#####################################################################################################################

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
# choose your font
font pango:Noto Mono Regular 13



#####################################################################################################################
#################                          assign applications to workspaces                       #################
#####################################################################################################################

# Assign application to start on a specific workspace
# you can find the class with the program xprop
#for_window [class="Terminator"] move to workspace $ws1
for_window [class="firefox|Firefox"] move to workspace $ws2 ; workspace $ws2
for_window [class="Clementine"] move to workspace $ws3 ; workspace $ws3
for_window [class="libreoffice-startcenter|libreoffice-writer"] move to workspace $ws4 ; workspace $ws4
for_window [class="Org.gnome.Nautilus"] move to workspace $ws5 ; workspace $ws5
for_window [class="showfoto"] move to workspace $ws6 ; workspace $ws6
for_window [class="Google-chrome"] move to workspace $ws7 ; workspace $ws7

#####################################################################################################################
#################           autostart   -    execute applications at boot time                      #################
#####################################################################################################################

# USER APPLICATIONS TO START AT BOOT



# TRAY APPLICATIONS

# applications that are not installed will not start
# you may see a wheel - hashtag out things you do not want

#get auth work with polkit-gnome
exec --no-startup-id /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1

# NetworkManager is the most popular way to manage wireless networks on Linux,
# and nm-applet is a desktop environment-independent system tray GUI for it.
exec --no-startup-id nm-applet

# Disable screensaver or set time
# xset s 3600 3600 	Change blank time to 1 hour 
# xset s off        Disable screen saver blanking
exec_always xset s off

# num lock activated
exec --no-startup-id numlockx on

# start conky: 
# exec_always --no-startup-id conky

# Swap mouse buttons using xmodmap
exec_always xmodmap -e "pointer = 3 2 1"

# Set Right Control as compose key
exec_always setxkbmap -option compose:rctrl

# Display wallpaper
exec_always nitrogen --restore

# Compositor
# exec_always compton -f
exec always picom --config_path $HOME/.config/picom.conf

# Updater
exec --no-startup-id pamac-tray

# Polybar launch script
# exec_always --no-startup-id $HOME/.config/polybar/launch.sh

#####################################################################################################################
#################                          applications keyboard shortcuts                          #################
#####################################################################################################################

#not workspace related

# start a terminal
bindsym $mod+Return exec terminator

# start other standard apps
bindsym XF86Messenger exec firefox
bindsym XF86Favorites exec google-chrome-stable
bindsym XF86HomePage exec nautilus
bindsym XF86Tools exec clementine
bindsym XF86Documents exec libreoffice

# Keybinding for i3lock
# Lock by blurring the screen
bindsym $mod+x exec ~/.config/i3/scripts/blur-lock.sh

#####################################################################################################################
#################                          screenshots                                              #################
#####################################################################################################################


bindsym Print exec --no-startup-id scrot '%Y-%m-%d-%s_screenshot_$wx$h.jpg' -e 'mv $f $$(xdg-user-dir PICTURES)'
#bindsym Control+Print exec --no-startup-id xfce4-screenshooter
#bindsym Control+Shift+Print exec --no-startup-id gnome-screenshot -i
#bindsym shift+Print exec --no-startup-id shutter

#####################################################################################################################
#################                          audio settings                                           #################
#####################################################################################################################

bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +2% && $refresh_i3status
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -2% && $refresh_i3status
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute @DEFAULT_SINK@ toggle && $refresh_i3status
bindsym XF86AudioMicMute exec --no-startup-id pactl set-source-mute @DEFAULT_SOURCE@ toggle && $refresh_i3status

#https://github.com/acrisci/playerctl/
bindsym XF86AudioPlay exec --no-startup-id playerctl play-pause
bindsym XF86AudioNext exec --no-startup-id playerctl next
bindsym XF86AudioPrev exec --no-startup-id playerctl previous
bindsym XF86AudioStop exec --no-startup-id playerctl stop
#bindsym XF86AudioPlay exec --no-startup-id "dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause"
#bindsym XF86AudioNext exec --no-startup-id "dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Next"
#bindsym XF86AudioPrev exec --no-startup-id "dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Previous"
#bindsym XF86AudioStop exec --no-startup-id "dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Stop"


#####################################################################################################################
#################                          border control                                           #################
#####################################################################################################################


# Border control
hide_edge_borders both
bindsym $mod+shift+b exec --no-startup-id i3-msg border toggle

#changing border style
#super+t in arcolinux is starting terminal
bindsym $mod+t border normal
bindsym $mod+y border 1pixel
bindsym $mod+u border none

#new_window pixel 1
new_window normal
#new_window none

#new_float pixel 1
new_float normal
#new_float none

#####################################################################################################################
#################                          Popups  control                                          #################
#####################################################################################################################

#Popups during fullscreen mode
popup_during_fullscreen smart

#####################################################################################################################
#################                          i3 gaps next                                             #################
#####################################################################################################################

# Settings for I3 next gap git
# https://github.com/Airblader/i3/tree/gaps-next
# delete or uncomment the following lines if you do not have it or do not
# want it


for_window [class="^.*"] border pixel 2
gaps inner 5
gaps outer 5
#smart_gaps on
#smart_borders on

#####################################################################################################################
#################                          i3 gaps change                                           #################
#####################################################################################################################

set $mode_gaps Gaps: (o) outer, (i) inner
set $mode_gaps_outer Outer Gaps: +|-|0 (local), Shift + +|-|0 (global)
set $mode_gaps_inner Inner Gaps: +|-|0 (local), Shift + +|-|0 (global)
bindsym $mod+Shift+g mode "$mode_gaps"

mode "$mode_gaps" {
        bindsym o      mode "$mode_gaps_outer"
        bindsym i      mode "$mode_gaps_inner"
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

mode "$mode_gaps_inner" {
        bindsym plus  gaps inner current plus 5
        bindsym minus gaps inner current minus 5
        bindsym 0     gaps inner current set 0

        bindsym Shift+plus  gaps inner all plus 5
        bindsym Shift+minus gaps inner all minus 5
        bindsym Shift+0     gaps inner all set 0

        bindsym Return mode "default"
        bindsym Escape mode "default"
}
mode "$mode_gaps_outer" {
        bindsym plus  gaps outer current plus 5
        bindsym minus gaps outer current minus 5
        bindsym 0     gaps outer current set 0

        bindsym Shift+plus  gaps outer all plus 5
        bindsym Shift+minus gaps outer all minus 5
        bindsym Shift+0     gaps outer all set 0

        bindsym Return mode "default"
        bindsym Escape mode "default"
}


#####################################################################################################################
#################                            Workspaces                                             #################
#####################################################################################################################


# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1: Terminal"
set $ws2 "2: Firefox"
set $ws3 "3: Audio"
set $ws4 "4: Office"
set $ws5 "5: Files"
set $ws6 "6: Foto"
set $ws7 "7: Chrome"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

# switch to workspace
bindsym $mod+1 workspace number $ws1
bindsym $mod+2 workspace number $ws2
bindsym $mod+3 workspace number $ws3
bindsym $mod+4 workspace number $ws4
bindsym $mod+5 workspace number $ws5
bindsym $mod+6 workspace number $ws6
bindsym $mod+7 workspace number $ws7
bindsym $mod+8 workspace number $ws8
bindsym $mod+9 workspace number $ws9
bindsym $mod+0 workspace number $ws10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace number $ws1
bindsym $mod+Shift+2 move container to workspace number $ws2
bindsym $mod+Shift+3 move container to workspace number $ws3
bindsym $mod+Shift+4 move container to workspace number $ws4
bindsym $mod+Shift+5 move container to workspace number $ws5
bindsym $mod+Shift+6 move container to workspace number $ws6
bindsym $mod+Shift+7 move container to workspace number $ws7
bindsym $mod+Shift+8 move container to workspace number $ws8
bindsym $mod+Shift+9 move container to workspace number $ws9
bindsym $mod+Shift+0 move container to workspace number $ws10


#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
#################                          bar appearance                                           #################
#####################################################################################################################
#####################################################################################################################
#####################################################################################################################

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
# Statusbar colors: see https://i3wm.org/docs/userguide.html#_colors
# For transparency see https://gist.github.com/lopspower/03fb1cc0ac9f32ef38f4

######################################
# color settings for bar and windows #
######################################

# Define colors variables: 
set $darkbluetrans      #08052be6
set $darkblue           #08052b
set $lightblue          #5294e2
set $urgentred          #e53935
set $white              #ffffff
set $black              #000000
set $purple             #e345ff
set $darkgrey           #383c4a
set $grey               #b0b5bd
set $mediumgrey         #8b8b8b
set $yellowbrown        #e1b700

# define colors for windows:
#class                          border          bground         text            indicator       child_border
client.focused              $lightblue  $darkblue       $white          $purple         $mediumgrey
client.unfocused            $darkblue   $darkblue       $grey           $purple         $darkgrey
client.focused_inactive $darkblue       $darkblue       $grey           $purple         $black
client.urgent               $urgentred  $urgentred      $white          $purple         $yellowbrown

############################################
# bar settings (input comes from i3blocks) #
############################################

# Start i3bar to display a workspace bar 
# (plus the system information i3status finds out, if available)

bar {
	font pango:Noto Sans Regular 10, FontAwesome 10
        status_command i3blocks -c ~/.config/i3/i3blocks.conf
        position top
        i3bar_command i3bar --transparency

# When strip_workspace_numbers is set to yes, 
# any workspace that has a name of the form 
# “[n][:][NAME]” will display only the name.
		strip_workspace_numbers no

        colors {
                 separator          $purple
                  background         $darkgrey
                  statusline         $white
#                                       border                  bg              txt             indicator
                focused_workspace       $mediumgrey             $grey           $darkgrey       $purple
                active_workspace        $lightblue              $mediumgrey     $darkgrey       $purple
                inactive_workspace      $darkgrey               $darkgrey       $grey           $purple
                urgent_workspace        $urgentred              $urgentred      $white          $purple
        }
}





                                        
